using Audio;
using GameManagement;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class CreditsManager : MonoBehaviour
    {
        public Button titleButton, exitButton;

        private AudioManager _audioManager;
        
        private void Start()
        {
            Cursor.lockState = CursorLockMode.None;
            titleButton.onClick.AddListener(LoadTitleScreen);
            exitButton.onClick.AddListener(ExitGame);
            SaveSystem.DeletePlayer();
            _audioManager = GetComponent<AudioManager>();
            _audioManager.Play("Credits");
        }

        private void LoadTitleScreen()
        {
            DisableButtons();
            _audioManager.Play("Button");
            _audioManager.Stop("Credits");
            LevelLoader.Instance.LoadLevel(0, true);
        }

        private void ExitGame()
        {
            DisableButtons();
            _audioManager.Play("Button");
            _audioManager.Stop("Credits");
            Application.Quit();
        }

        private void DisableButtons()
        {
            titleButton.enabled = false;
            exitButton.enabled = false;
        }
    }
}
