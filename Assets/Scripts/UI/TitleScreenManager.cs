using System.IO;
using Audio;
using GameManagement;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class TitleScreenManager : MonoBehaviour
    {
        public Button newGameButton, continueButton, exitButton;

        private AudioManager _audioManager;

        private bool _saveExists;

        private void Start()
        {
            Cursor.lockState = CursorLockMode.None;
            newGameButton.onClick.AddListener(StartGame);
            _saveExists = File.Exists(GameManager.Instance.savePath);
            continueButton.onClick.AddListener(LoadGame);

            if (!_saveExists)
            {
                continueButton.GetComponent<Image>().color = Color.gray;
            }

            exitButton.onClick.AddListener(ExitGame);
            _audioManager = GetComponent<AudioManager>();
            _audioManager.Play("TitleScreen");
        }

        private void StartGame()
        {
            DisableButtons();
            _audioManager.Play("Button");
            _audioManager.Stop("TitleScreen");
            SaveSystem.DeletePlayer();
            LevelLoader.Instance.LoadLevel(1, false);
        }

        private void LoadGame()
        {
            if (!_saveExists) return;
            DisableButtons();
            _audioManager.Play("Button");
            _audioManager.Stop("TitleScreen");
            LevelLoader.Instance.LoadLevel(1, false);
        }

        private void ExitGame()
        {
            DisableButtons();
            _audioManager.Play("Button");
            _audioManager.Stop("TitleScreen");
            Application.Quit();
        }

        private void DisableButtons()
        {
            newGameButton.enabled = false;
            continueButton.enabled = false;
            exitButton.enabled = false;
        }
    }
}