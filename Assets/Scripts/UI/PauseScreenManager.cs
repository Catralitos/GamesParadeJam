using Audio;
using GameManagement;
using Player;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace UI
{
    public class PauseScreenManager : MonoBehaviour
    {
        public static bool gameIsPaused;

        public GameObject pauseMenuUI;
        //public Button resumeButton, returnButton, exitButton;

        public TextMeshProUGUI returnButtonText;
        private AudioManager _audioManager;
        private CursorLockMode _previous;

        private void Start()
        {
            _audioManager = GetComponent<AudioManager>();
            pauseMenuUI.SetActive(false);
            //resumeButton.onClick.AddListener(Resume);
            //returnButton.onClick.AddListener(Return);
            //exitButton.onClick.AddListener(ExitGame);
        }

        private void Update()
        {
            returnButtonText.text =
                SceneManager.GetActiveScene().buildIndex == 1 ? "Return to  Title" : "Return to House";
            if (!Input.GetKeyDown(KeyCode.Escape)) return;
            if (gameIsPaused) Resume();
            else Pause();
        }

        public void Resume()
        {
            pauseMenuUI.SetActive(false);
            Cursor.lockState = _previous;
            Time.timeScale = 1f;
            gameIsPaused = false;
        }

        private void Pause()
        {
            pauseMenuUI.SetActive(true);
            _previous = Cursor.lockState;
            Cursor.lockState = CursorLockMode.None;
            Time.timeScale = 0f;
            gameIsPaused = true;
        }

        public void Return()
        {
            Debug.Log("Chamou o return");
            _audioManager.Play("Button");
            Resume();
            if (SceneManager.GetActiveScene().buildIndex == 1)
            {
                LevelLoader.Instance.LoadLevel(0, true);
                Destroy(gameObject);
            }
            else
            {
                LevelLoader.Instance.LoadLevel(1, false);
            }
        }

        public void ExitGame()
        {
            Debug.Log("Chamou o exit game");
            _audioManager.Play("Button");
            Application.Quit();
        }
    }
}