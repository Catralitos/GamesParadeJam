using System.Collections;
using System.Collections.Generic;
using GameManagement;
using UnityEngine;

public class Portal : MonoBehaviour
{
    public GameObject leftPivot;
    public GameObject rightPivot;
    public Light light;
    public float openSpeed;
    bool _open = false;

    public void Open() {
        if(GameManagement.LevelManager.Instance.IsCompleted()) {
            _open = true;
            LevelManager.Instance.StopAllSounds();
            Invoke(nameof(LoadEnd), 5);
        }
    }

    private void Update() {
        if (_open)
        {
            light.intensity += light.intensity * 0.01f + 1;
            leftPivot.transform.rotation = Quaternion.Lerp(leftPivot.transform.rotation,
                Quaternion.Euler(leftPivot.transform.rotation.eulerAngles.x, 90, leftPivot.transform.rotation.eulerAngles.z), Time.deltaTime * openSpeed);
            rightPivot.transform.rotation = Quaternion.Lerp(rightPivot.transform.rotation,
                Quaternion.Euler(rightPivot.transform.rotation.eulerAngles.x, -90, rightPivot.transform.rotation.eulerAngles.z), Time.deltaTime * openSpeed);
        }
    }

    void LoadEnd() {
        Destroy(LevelManager.Instance.gameObject);
        GameManagement.LevelLoader.Instance.LoadLevel(5, true);
    }
}
