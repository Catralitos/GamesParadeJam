using System;
using System.Collections;
using Audio;
using UnityEngine;

public class Target : MonoBehaviour
{
    public float moveSpeed;
    public float dropSpeed;
    public float destroyTime;

    public bool goingLeft;

    public int value;

    private bool _hit;
    private Rigidbody _rb;

    Vector3 dir;

    // Start is called before the first frame update
    private void Start()
    {
        //talvez mudar este para getinchildren
        _rb = GetComponent<Rigidbody>();
        Invoke(nameof(DestroyTarget), destroyTime);
        dir = goingLeft ? Vector3.left : Vector3.right;
        _hit = false;
    }

    private void Update()
    {
        if (_hit)
        {
            transform.rotation = Quaternion.Lerp(transform.rotation,
                Quaternion.Euler(-90, transform.rotation.eulerAngles.y, transform.rotation.eulerAngles.z),
                Time.deltaTime * dropSpeed);
        }
        transform.Translate(dir * moveSpeed * Time.deltaTime);
    }

    public void Hit()
    {
        ShootingGalleryManager.instance.AddScore(value);
        _hit = true;
        GetComponent<AudioManager>().Play("Hit");
    }


    private void DestroyTarget()
    {
        Destroy(gameObject);
    }
}