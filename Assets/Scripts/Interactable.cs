using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;

[RequireComponent(typeof(Outline))]
public class Interactable : MonoBehaviour
{
    public UnityEvent onInteraction;
    static bool has_tested;
    bool isHighLighted = false;

    Outline outline;
    void Start(){
        outline = GetComponent<Outline>();
    }

    void Update()
    {
        if(!has_tested) {
            ShootRay();
        }
    }

    void LateUpdate() {
        if(isHighLighted) {
            outline.OutlineWidth = 10;
            if(Input.GetKeyDown(KeyCode.E)) {
                Interact();
            }
        }
        else {
            outline.OutlineWidth = 0;
        }
        has_tested = false;    
        isHighLighted = false;
    }

    void HighLight() {
        isHighLighted = true;
    }
    
    void Interact() {
        onInteraction.Invoke();
    }

    static void ShootRay() {
        Ray ray = Camera.main.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit)) {
            if (LayerMask.NameToLayer("Interactable") == hit.transform.gameObject.layer) {
                Interactable inter = hit.transform.gameObject.GetComponent<Interactable>();
                inter.HighLight();
            }
        }
        has_tested = true;
    }
}
