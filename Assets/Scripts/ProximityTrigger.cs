using System.Collections;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class ProximityTrigger : MonoBehaviour
{
    public TextMesh target;
    public LayerMask layers;

    private bool _inside;

    public float fadeDuration;
    public float disableTime = 5.0f;

    private void Start()
    {
        _inside = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!Extensions.LayerMaskExtensions.HasLayer(layers, other.gameObject.layer) || _inside) return;
        _inside = true;
        StartCoroutine(StartFadeIn());
    }

    private void OnTriggerExit(Collider other)
    {
        if (!Extensions.LayerMaskExtensions.HasLayer(layers, other.gameObject.layer) || !_inside) return;
        Invoke(nameof(DisableObject), disableTime);
    }

    private void DisableObject()
    {
        _inside = false;
        StartCoroutine(StartFadeOut());
    }

    private IEnumerator StartFadeIn()
    {
        float currentTime = 0;

        while (currentTime < fadeDuration)
        {
            currentTime += Time.deltaTime;

            float alpha = Mathf.Lerp(0, 1, currentTime / fadeDuration);
            target.color = new Color(1, 1, 1, alpha);
            yield return null;
        }
    }

    private IEnumerator StartFadeOut()
    {
        float currentTime = 0;

        while (currentTime < fadeDuration)
        {
            currentTime += Time.deltaTime;

            float alpha = Mathf.Lerp(1, 0, currentTime / fadeDuration);
            target.color = new Color(1, 1, 1, alpha);
            yield return null;
        }
    }
}