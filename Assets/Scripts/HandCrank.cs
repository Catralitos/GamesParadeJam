using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandCrank : MonoBehaviour
{
    public bool isPlane = false;
    float maxAngle = 5;
    Vector3 lastDisplacement = Vector3.up;

    bool grabbed = false;
    public void setGrabbed() {
        lastDisplacement = Input.mousePosition - Camera.main.WorldToScreenPoint(transform.position);
        grabbed = true;
    }

    void Update()
    {   
        if(Input.GetMouseButtonUp(0)) grabbed = false;
        if (grabbed){
            Vector3 displacement = Input.mousePosition - Camera.main.WorldToScreenPoint(transform.position);
            float angle = Vector3.SignedAngle(lastDisplacement, displacement.normalized, Vector3.forward);
            angle = Mathf.Clamp(angle, -maxAngle, 0);
            transform.Rotate(0,0,angle);
            lastDisplacement = displacement;
            if(isPlane) {
                PlaneController.instance.PowerUp(-angle);
            }
            else {
                PlaneObstacles.instance.Move(-angle);
            }
        }
    }
}
