using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitText : MonoBehaviour
{

    public int vignetteNumber;

    void Start()
    {
        bool completed = false;
        switch (vignetteNumber)
        {
            case(1):
                completed = GameManagement.LevelManager.Instance.firstVignette;
                break;
            case(2):
                completed = GameManagement.LevelManager.Instance.secondVignette;
                break;
            case(3):
                completed = GameManagement.LevelManager.Instance.thirdVignette;
                break;
        }
        if(completed) {
            this.gameObject.SetActive(false);
        }
        else
        {
            Invoke(nameof(Hide), 10);
        }   
    }

    void Hide() {
        this.gameObject.SetActive(false);
    }
}
