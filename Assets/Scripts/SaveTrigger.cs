using System.Collections;
using System.Collections.Generic;
using GameManagement;
using Player;
using UnityEngine;

public class SaveTrigger : MonoBehaviour
{
    public LayerMask layers;
    private void OnTriggerEnter(Collider other)
    {
        if (!Extensions.LayerMaskExtensions.HasLayer(layers, other.gameObject.layer)) return;
        SaveSystem.SavePlayer();
    }
    
    private void OnTriggerExit(Collider other)
    {
        if (!Extensions.LayerMaskExtensions.HasLayer(layers, other.gameObject.layer)) return;
        SaveSystem.SavePlayer();
    }
}
