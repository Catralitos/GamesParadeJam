using System.Collections;
using System.Collections.Generic;
using Audio;
using GameManagement;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Rigidbody))]
public class PlaneController : MonoBehaviour
{
    public static PlaneController instance;

    public float maxEnergy = 200;
    public float energyDecay = 10;
    float energy = 0;

    public float speed = 400;
    public float fallingSpeed = 200;
    Rigidbody rb;
    //MeshRenderer mr;
    public Image energyBar; 
    Vector3 originalPos;

    private AudioManager _audioManager;
    private void Awake() {
        instance = this;
        originalPos = transform.position;
    }
    void Start()
    {
        //mr = GetComponent<MeshRenderer>();
        rb = GetComponent<Rigidbody>();
        _audioManager = GetComponent<AudioManager>();
    }

    void Respawn() {
        Vector3 testPos = originalPos;
        Vector3 extends = new Vector3(2, 1, 1);
        while(Physics.OverlapBox(testPos, extends).Length > 0) {
            testPos.x += Random.Range(-0.5f, 0.5f);
            testPos.y += Random.Range(-0.5f, 0.5f);
        }
        transform.position = testPos;
    }

    private void Update() {
        //mr.material.color = new Color(0, energy/maxEnergy, 0);

        if (energy > 0)
        {
            _audioManager.Play("Engine");
        }
        else
        {
            _audioManager.Stop("Engine");
        }

        if(Camera.main.WorldToScreenPoint(transform.position + new Vector3(5, 0, 0)).x < 0) {
            Respawn();
        }

        if (Input.GetMouseButtonDown(0)) {
            RaycastHit hit = new RaycastHit();


        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray.origin,ray.direction, out hit, 1000)) {
                if(hit.transform.gameObject.tag == "HandCrankHandle") {
                    hit.transform.gameObject.GetComponent<HandCrankHandle>().OnGrab();
                }
            }
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {   
        energy -= energyDecay * Time.fixedDeltaTime;
        energy = Mathf.Clamp(energy, 0, maxEnergy);
        energyBar.fillAmount = energy/maxEnergy;
        if(energy > 0) {
            Vector3 movementDirection = new Vector3();
            if(Input.GetKey(KeyCode.W)){
                movementDirection.y = 1;
            }
            
            if(Input.GetKey(KeyCode.S)){
                movementDirection.y -= 1;
            }
            
            if(Input.GetKey(KeyCode.D)){
                movementDirection.x = 1;
            }
            
            if(Input.GetKey(KeyCode.A)){
                movementDirection.x -= 1;
            }

            movementDirection.Normalize();
            rb.velocity = movementDirection * speed * Time.fixedDeltaTime;
        }
        else {
            rb.velocity = Vector3.down * fallingSpeed * Time.fixedDeltaTime;
        }
    }

    public void Win() {
        GetComponent<VignetteEnder>().LoadHouse();
        Debug.Log("You win");
    }

    public void PowerUp(float value) {
        energy += value;
    }

}
