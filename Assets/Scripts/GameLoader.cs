using GameManagement;
using Player;
using Unity.Mathematics;
using UnityEngine;

public class GameLoader : MonoBehaviour
{
    private void Start()
    {
        PlayerData savedData = SaveSystem.LoadPlayer();
        if (savedData == null) return;
        var o = PlayerEntity.Instance.gameObject;
        o.transform.position = new Vector3(savedData.positionX, savedData.positionY, savedData.positionZ);
        o.transform.rotation = quaternion.Euler(savedData.rotationX, savedData.rotationY, savedData.rotationZ);
        if (!LevelManager.Instance.loadedVignette)
        {
            Debug.Log(savedData.firstVignette + " " + savedData.secondVignette + " " + savedData.thirdVignette);
            LevelManager.Instance.firstVignette = savedData.firstVignette == 1;
            LevelManager.Instance.secondVignette = savedData.secondVignette == 1;
            LevelManager.Instance.thirdVignette = savedData.thirdVignette == 1;
        }

        SaveSystem.SavePlayer();
    }
}