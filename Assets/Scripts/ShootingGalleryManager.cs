using GameManagement;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ShootingGalleryManager : MonoBehaviour
{
    public static ShootingGalleryManager instance;
    public ScoreTarget[] targets;
    public TextMeshProUGUI text;
    public TextMeshProUGUI goalText;
    public TextMeshProUGUI scoreText;
    int score = 0;
    int currentTarget = 0;

    public int winCondition;

    void Awake() {
        instance = this;
    }

    void Start() {
        UpdateDisplay();
    }

    void UpdateDisplay() {
        
        scoreText.text = "Score: " + score;
        if(currentTarget  < targets.Length) {
            goalText.text = "Goal: " + targets[currentTarget].scoreNeeded;
        }
        else {
            goalText.text = "No More Goals";
        }
        
    }

    public void AddScore(int increment) {
        score += increment;
        if(currentTarget  < targets.Length) {
            if(score >= targets[currentTarget].scoreNeeded){
                text.text = targets[currentTarget].textToDisplay;
                currentTarget++;
            }
        }
        if(score >= winCondition) {
            Invoke(nameof(Win), 5f);
        }
        UpdateDisplay();
    }

    void Win(){
        GetComponent<VignetteEnder>().LoadHouse();
    }
}

[System.Serializable]
public class ScoreTarget {
    public int scoreNeeded;
    [TextArea(3, 10)]
    public string textToDisplay;
}