using Audio;
using UnityEngine;

public class PlayAmbience : MonoBehaviour
{
    private void Start()
    {
        GetComponent<AudioManager>().Play("Ambience");
    }
    
}
