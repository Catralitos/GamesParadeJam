using UnityEngine;

public class ParticleTrigger : MonoBehaviour
{
    public LayerMask layers;

    public GameObject particles;
    
    private bool _inside;
    
    private void Start()
    {
        _inside = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!Extensions.LayerMaskExtensions.HasLayer(layers, other.gameObject.layer) || _inside) return;
        _inside = true;
        particles.SetActive(true);
    }

    private void OnTriggerExit(Collider other)
    {
        if (!Extensions.LayerMaskExtensions.HasLayer(layers, other.gameObject.layer) || !_inside) return;
        particles.SetActive(false);

    }
}
