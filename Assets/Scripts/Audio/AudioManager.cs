using System;
using System.Collections;
using UnityEngine;

namespace Audio
{
    public class AudioManager : MonoBehaviour
    {
        public float fadeDuration = 5f;
        public Sound[] sounds;

        private void Awake()
        {
            foreach (Sound s in sounds)
            {
                s.SetSource(gameObject.AddComponent<AudioSource>());
            }
        }

        public void Play(string soundName)
        {
            Sound s = Array.Find(sounds, sound => sound.name == soundName);
            if (s == null || s.source == null)
            {
                Debug.LogWarning("Sound " + soundName + " not found!");
                return;
            }

            if (IsPlaying(s.name)) return;

            if (s.fadeIn)
            {
                StartCoroutine(StartFadeIn(s.source, fadeDuration, s.volume));
            }

            s.Play();
        }

        public void Stop(string soundName)
        {
            Sound s = Array.Find(sounds, sound => sound.name == soundName);
            if (s == null || s.source == null)
            {
                Debug.LogWarning("Sound " + soundName + " not found!");
                return;
            }

            if (!IsPlaying(s.name)) return;
            if (s.fadeOut)
            {
                StartCoroutine(StartFadeOut(s.source, fadeDuration, 0.0f));
            }
            else
            {
                s.Stop();
            }
        }

        public void StopAll()
        {
            foreach (Sound s in sounds)
            {
                Stop(s.name);
            }
        }

        public void ChangeVolume(string soundName, float newVolume, bool fade)
        {
            Sound s = Array.Find(sounds, sound => sound.name == soundName);
            if (fade)
            {
                //fade out
                StartCoroutine(s.source.volume > newVolume
                    ? StartFadeOut(s.source, fadeDuration, newVolume)
                    : StartFadeIn(s.source, fadeDuration, newVolume));
            }
            else
            {
                s.source.volume = newVolume;
            }
        }

        private static IEnumerator StartFadeIn(AudioSource audioSource, float duration, float targetVolume)
        {
            float currentTime = 0;
            const float start = 0;

            while (currentTime < duration)
            {
                currentTime += Time.deltaTime;
                audioSource.volume = Mathf.Lerp(start, targetVolume, currentTime / duration);
                yield return null;
            }
        }

        private static IEnumerator StartFadeOut(AudioSource audioSource, float duration, float targetVolume)
        {
            float currentTime = 0;
            float start = audioSource.volume;

            while (currentTime < duration)
            {
                currentTime += Time.deltaTime;
                audioSource.volume = Mathf.Lerp(start, targetVolume, currentTime / duration);
                if (Mathf.Abs(audioSource.volume - 0.0f) <= 0.01f) audioSource.Stop();
                yield return null;
            }
        }

        private bool IsPlaying(string soundName)
        {
            Sound s = Array.Find(sounds, sound => sound.name == soundName);
            if (s != null) return s.IsPlaying();
            Debug.LogWarning("Sound " + soundName + " not found!");
            return false;
        }
    }
}