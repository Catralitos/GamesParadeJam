using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinishLine : MonoBehaviour
{
    private void OnTriggerEnter(Collider other) {
        PlaneController pc = other.transform.GetComponent<PlaneController>();
        if(pc != null) {
            pc.Win();
        }
    }
}
