using UnityEngine;

public class HandCrankHandle : MonoBehaviour
{
    HandCrank parentCrank;

    private void Start() {
        parentCrank = transform.parent.GetComponent<HandCrank>();
    }

    public void OnGrab() {
        parentCrank.setGrabbed();
    }
}