using UnityEngine;

public class TargetSpawner : MonoBehaviour
{
    public GameObject toSpawn;
    public float spawnCooldown;

    private float _cooldown;

    private void Start()
    {
        _cooldown = 0;
    }

    private void Update()
    {
        _cooldown -= Time.deltaTime;
        if (!(_cooldown <= 0.0f)) return;
        Instantiate(toSpawn, transform.position, transform.rotation);
        _cooldown = spawnCooldown;
    }
}