using Audio;
using UnityEngine;

namespace GameManagement
{
    public class LevelManager : MonoBehaviour
    {
        public static LevelManager Instance { get; private set; }

        public bool firstVignette;
        public bool secondVignette;
        public bool thirdVignette;

        public float musicVolume = 0.2f;
        private AudioManager _audioManager;

        public bool loadedVignette;

        public void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            else
            {
                Debug.LogWarning("Multiple level managers present in scene! Destroying...");
                Destroy(gameObject);
            }

            DontDestroyOnLoad(gameObject);
        }

        public void Start()
        {
            _audioManager = GetComponent<AudioManager>();
            _audioManager.Play("Violin");
            _audioManager.Play("Harp");
            _audioManager.Play("Cello");
            loadedVignette = true;
        }

        public void Update()
        {
            if (firstVignette)
            {
                _audioManager.ChangeVolume("Violin", musicVolume, true);
            }

            if (secondVignette)
            {
                _audioManager.ChangeVolume("Harp", musicVolume, true);
            }

            if (thirdVignette)
            {
                _audioManager.ChangeVolume("Cello", musicVolume, true);
            }
        }

        public void StopAllSounds()
        {
            _audioManager.StopAll();
        }

        public bool IsCompleted()
        {
            return firstVignette && secondVignette && thirdVignette;
        }
    }
}