using Player;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace GameManagement
{
    public class VignetteStarter : MonoBehaviour
    {
        public int vignetteSceneIndex;

        public void LoadVignette()
        {
            if (SceneManager.GetActiveScene().buildIndex == 1)
            {
                SaveSystem.SavePlayer();
                //LevelManager.Instance.lastPlayerPosition = PlayerEntity.Instance.gameObject.transform;
                LevelManager.Instance.loadedVignette = true;
            }
            LevelLoader.Instance.LoadLevel(vignetteSceneIndex, false);
        }
    }
}
