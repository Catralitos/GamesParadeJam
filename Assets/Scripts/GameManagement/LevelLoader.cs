using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace GameManagement
{
    public class LevelLoader : MonoBehaviour
    {
        public static LevelLoader Instance { get; private set; }

        public Animator animator;

        public GameObject buttonPrompt;

        public void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            else
            {
                Debug.LogWarning("Multiple level loaders present in scene! Destroying...");
                Destroy(gameObject);
            }

            DontDestroyOnLoad(gameObject);
        }

        public void LoadLevel(int sceneIndex, bool immediate)
        {
            if (!immediate)
            {
                animator.SetTrigger("FadeIn");
            }

            StartCoroutine(LoadAsynchronously(sceneIndex, immediate));
        }

        private IEnumerator LoadAsynchronously(int sceneIndex, bool immediate)
        {
            AsyncOperation operation = SceneManager.LoadSceneAsync(sceneIndex);

            if (!immediate)
            {
                operation.allowSceneActivation = false;
            }

            while (!operation.isDone)
            {
                if (operation.progress >= 0.9f)
                {
    
                    if (!immediate)
                    {
                        buttonPrompt.SetActive(true);
                        if (Input.GetKeyDown(KeyCode.Mouse0))
                        {
                            operation.allowSceneActivation = true;
                            animator.SetTrigger("FadeOut");
                            buttonPrompt.SetActive(false);
                            yield break;
                        }
                    }
                }

                yield return null;
            }
        }
    }
}