using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using Player;
using UnityEngine;

namespace GameManagement
{
    public static class SaveSystem
    {
        public static void SavePlayer()
        {
            BinaryFormatter formatter = new BinaryFormatter();
            string path = GameManager.Instance.savePath;
            //Application.persistentDataPath + "/player.house";
            FileStream stream = new FileStream(path, FileMode.Create);

            PlayerData data = new PlayerData();

            formatter.Serialize(stream, data);

            stream.Close();

            Debug.Log("Save file created at " + path);
        }

        public static PlayerData LoadPlayer()
        {
            string path = GameManager.Instance.savePath;
            if (File.Exists(path))
            {
                BinaryFormatter formatter = new BinaryFormatter();
                FileStream stream = new FileStream(path, FileMode.Open);
                PlayerData data = formatter.Deserialize(stream) as PlayerData;
                stream.Close();
                return data;
            }
            else
            {
                Debug.Log("Save file not found at " + path);
                return null;
            }
        }

        public static void DeletePlayer()
        {
            string path = GameManager.Instance.savePath;
            if (File.Exists(path))
            {
                File.Delete(path);
            }
        }
    }
}