using Audio;
using UnityEngine;

namespace GameManagement
{
    public class GameManager : MonoBehaviour
    {
        public static GameManager Instance { get; private set; }
        [HideInInspector] public string savePath;

        private AudioManager _audioManager;
        
        public void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
                savePath = Application.persistentDataPath + "/player.house";
            }
            else
            {
                Debug.LogWarning("Multiple game managers present in scene! Destroying...");
                Destroy(gameObject);
            }
            DontDestroyOnLoad(gameObject);
            
        }
        
    }
}
