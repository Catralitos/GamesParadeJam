using UnityEngine;

namespace GameManagement
{
    public class VignetteEnder : MonoBehaviour
    {
        public int vignetteNumber;
    
        public void LoadHouse()
        {
            switch (vignetteNumber)
            {
                case 1:
                    LevelManager.Instance.firstVignette = true;
                    break;
                case 2:
                    LevelManager.Instance.secondVignette = true;
                    break;
                case 3:
                    LevelManager.Instance.thirdVignette = true;
                    break;
                default:
                    Debug.Log("Não meteu nenhuma bool true");
                    break;
            }
            LevelLoader.Instance.LoadLevel(1, false);
        }
    }
}
