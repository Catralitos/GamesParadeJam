using Player;
using UnityEngine;

namespace GameManagement
{
    [System.Serializable]
    public class PlayerData
    {
        public float positionX;
        public float positionY;
        public float positionZ;
        public float rotationX;
        public float rotationY;
        public float rotationZ;
        public int firstVignette;
        public int secondVignette;
        public int thirdVignette;

        public PlayerData()
        {
            Transform p = PlayerEntity.Instance.gameObject.transform;
            Vector3 pos = p.position;
            Vector3 rot = p.rotation.eulerAngles;
            positionX = pos.x;
            positionY = pos.y;
            positionZ = pos.z;
            rotationX = rot.x;
            rotationY = rot.y;
            rotationZ = rot.z;

            firstVignette = LevelManager.Instance.firstVignette ? 1 : 0;
            secondVignette = LevelManager.Instance.secondVignette ? 1 : 0;
            thirdVignette = LevelManager.Instance.thirdVignette ? 1 : 0;

            /*Debug.Log("Saved PlayerData with: ");
            Debug.Log("Pos X: " + positionX);
            Debug.Log("Pos Y: " + positionY);
            Debug.Log("Pos Z: " + positionZ);
            Debug.Log("Rot X: " + rotationX);
            Debug.Log("Rot Y: " + rotationX);
            Debug.Log("Rot Z: " + rotationX);
            Debug.Log("First: " + firstVignette);
            Debug.Log("Second: " + secondVignette);
            Debug.Log("Third: " + thirdVignette);*/
        }
    }
}