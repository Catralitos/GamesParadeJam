using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaneObstacles : MonoBehaviour
{
    public static PlaneObstacles instance;
    public float speed = 20;

    private void Awake() {
        instance = this;    
    }
    public void Move(float value)
    {
        transform.Translate(value * Vector3.left * speed * Time.deltaTime);
    }
}
